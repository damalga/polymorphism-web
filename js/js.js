//tilt
$('.glare').tilt({
    glare: true,
    maxGlare: .5
});

//tiny-slider
var slider1 = tns({
    container: '.tns-1',
    mouseDrag: true
});
var slider2 = tns({
    container: '.tns-2',
    mouseDrag: true
});
var slider3 = tns({
    container: '.tns-3',
    mouseDrag: true
});
var slider4 = tns({
    container: '.tns-4',
    mouseDrag: true
});

$('.release').click(function () {
    $('.releases .right-image').addClass('disp-none');
    $('.releases .right-image[data-index="' + $(this).data('index') + '"]').removeClass('disp-none');
    $('.release').removeClass('active');
    $(this).toggleClass('active');
    $('.embed').addClass('disp-none');
    $('.embed[data-index="' + $(this).data('index') + '"]').removeClass('disp-none');
});
$('.showcase').click(function () {
    $('.showcases .right-image').addClass('disp-none');
    $('.showcases .right-image[data-index="' + $(this).data('index') + '"]').removeClass('disp-none');
    $('.showcase').removeClass('active');
    $(this).toggleClass('active');
});

$('.showcase').click(function () {
    var index = $(this).data('index');
    $('.showcases .right-image').addClass('disp-none');
    $('.showcases .right-image[data-index="' + index + '"]').removeClass('disp-none');
    $('.showcase').removeClass('active');
    $(this).addClass('active');
});

//move main the header height
function moveMain() {
    const header = document.getElementById('header');
    const headerHeight = header.offsetHeight;

    const main = document.getElementById('main');
    main.style.top = headerHeight + 'px';
}
moveMain();

//position fixed sectionLeft showcases
function fixedShow() {
    const header = document.getElementById('header');
    const headerHeight = header.offsetHeight;
    const sectionReleases = document.getElementById('releases-section');
    const sectionReleasesHeight = sectionReleases.offsetHeight;
    const sectionShowcasesLeftSection = document.querySelector('.showcases .left');

    const updatePosition = () => {
        const scrollY = window.scrollY || window.pageYOffset;
        if (scrollY > sectionReleasesHeight) {
            sectionShowcasesLeftSection.classList.add('fixed');
            sectionShowcasesLeftSection.style.top = headerHeight + 'px';
        } else {
            sectionShowcasesLeftSection.classList.remove('fixed');
            sectionShowcasesLeftSection.style.top = null;
        }
    };

    const checkWidthAndPosition = () => {
        if (window.innerWidth > 768) {
            updatePosition();
            window.addEventListener('scroll', updatePosition);
        } else {
            sectionShowcasesLeftSection.classList.remove('fixed');
            sectionShowcasesLeftSection.style.top = null;
            window.removeEventListener('scroll', updatePosition);
        }
    };

    checkWidthAndPosition(); // Call initially

    window.addEventListener('resize', () => {
        // On resize, check width and position
        checkWidthAndPosition();
        moveMain();
    });
}

fixedShow();


//lenis
const lenis = new Lenis({
    duration: 1.12,
    easing: (t) => Math.min(1, 1.001 - Math.pow(2, -8 * t)), // https://www.desmos.com/calculator/brs54l4xou
    direction: 'vertical', // vertical, horizontal
    gestureDirection: 'vertical', // vertical, horizontal, both
    smooth: true,
    mouseMultiplier: .96,
    smoothTouch: false,
    touchMultiplier: .96,
    infinite: false,
})

function raf(time) {
    lenis.raf(time);
    requestAnimationFrame(raf);
}

requestAnimationFrame(raf);